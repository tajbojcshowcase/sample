from django.db import models
import uuid
from django.urls import reverse

# Create your models here.
class Kategorije(models.Model):
    naziv_kategorije = models.CharField(max_length=200, help_text="Naziv Kategorije")

    class Meta:
        verbose_name_plural = "Kategorije"

    def __str__(self):
        return self.naziv_kategorije
    
    def get_absolute_url(self):
        return reverse('kategorije')
    
class Podkategorije(models.Model):
    naziv_podkategorije = models.CharField(max_length=200)
    opis = models.TextField(max_length=4000)
    veza = models.ForeignKey('Kategorije', on_delete=models.SET_NULL, null=True)

    class Meta:
        verbose_name_plural = "Podkategorije"

    def __str__(self):
        return self.naziv_podkategorije
    
    def get_absolute_url(self):
        return reverse('kategorije')
    
class Proizvodi(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    naziv_proizvoda = models.CharField(max_length=200)
    opis_proizvoda = models.TextField(max_length=1000)
    Podkategorije_veza = models.ForeignKey('Podkategorije', on_delete=models.SET_NULL, null=True)
    kupac_veza = models.ForeignKey('Kupci', on_delete=models.SET_NULL, null=True, blank=True) 


    def __str__(self):
        return self.title 

    class Meta:
        verbose_name_plural = "Proizvodi"


    
    def get_absolute_url(self):
        return reverse('proizvodi')




    

class Kupci(models.Model):
    ime = models.CharField(max_length=200)
    prezime = models.CharField(max_length=200)
    telefon = models.CharField(max_length=20)
    email = models.EmailField()
    adresa = models.TextField(max_length=500)
    primjedba = models.TextField(max_length=1000)


    

    
    class Meta:
        verbose_name_plural = "Kupci"
    
        def __str__(self):
           return '{}{}{}'.format("self.ime, self.prezime")
        
        
        
        
        

        
     


    

        
    
    
    
    
    
    
    
    
    
    

        